<?php
namespace Blog;

class Config
{
    /**
     * @var string
     */
    const DB_HOST = 'localhost';
    /**
     * @var string
     */
    const DB_USER = 'root';
    /**
     * @var string
     */
    const DB_PASSWORD = '';
    /**
     * @var string
     */
    const DB_NAME = 'hyperiablog';
    /**
     * @var boolean
     */
    const SHOW_ERRORS = true;
}