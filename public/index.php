<?php
require_once('../config/Model.php');
require_once('../src/Models/Comment.php');
require_once('../src/Models/Tag.php');
require_once('../src/Models/Author.php');
require_once('../src/Models/Article.php');

require_once('../src/Controllers/Blog.php');
?>
<html>
<head>
    <title>Hyperia Blog</title>
    <link rel="stylesheet" type="text/css" href="css/blog.css" />
</head>
<body>
<a href="/"><h1 class="headline">Hyperia Blog</h1></a>

<?php
    include 'router.php';
?>

</body>