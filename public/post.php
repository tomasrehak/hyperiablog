<?php
require_once('../config/Model.php');
require_once('../src/Models/Comment.php');
require_once('../src/Models/Tag.php');
require_once('../src/Models/Author.php');
require_once('../src/Models/Article.php');

require_once('../src/Controllers/Blog.php');

$request = $_SERVER['REQUEST_URI'];
$action = $_POST['action'];

switch ($action) {
    case "add-tag":
        $tag = $_POST['name'];
        $articleId = $_POST['articleId'];
        $blogController = new \Blog\Controllers\Blog();
        $response = $blogController->createTagForArticle($tag, $articleId);
        break;
    case "search-tag":
        $tag = $_POST['name'];
        $blogController = new \Blog\Controllers\Blog();
        $response = $blogController->searchArticlesByTag($tag);
        break;
    case "send-comment":
        $name = $_POST['name'];
        $text = $_POST['text'];
        $articleId = $_POST['articleId'];
        $blogController = new \Blog\Controllers\Blog();
        $response = $blogController->saveCommentToArticle($name, $text, $articleId);
        break;
}

echo json_encode($response);