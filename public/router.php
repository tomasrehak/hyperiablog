<?php

$request = $_SERVER['REQUEST_URI'];
$page = strtok($request, '?');

switch ($page) {
    case '/' :
        require '../src/Views/Blog.php';
        break;
    case '' :
        require '../src/Views/Blog.php';
        break;
    case '/article' :
        require '../src/Views/Article.php';
        break;
    default:
        require '../src/Views/404.php';
        break;
}