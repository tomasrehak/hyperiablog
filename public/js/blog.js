function searchTag(element) {
    const text = element.value;
    let xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (this.readyState === 4 && this.status === 200) {
            var data = JSON.parse(xmlhttp.responseText);
            var articlesToShow = data.map((article) => {
                return 'article-'+article.article_id;
            });
            var articles = document.getElementsByClassName('article');
            var arr = Array.prototype.slice.call(articles);
            arr.forEach(item => {
                if (articlesToShow.indexOf(item.id) === -1) {
                    item.style.display = 'none';
                } else {
                    item.style.display = 'block';
                }
            });
        }
    };
    xmlhttp.open("POST", "post.php", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send('action=search-tag&name='+text);
};

function addTag(articleId, event) {
    event.preventDefault();
    const text = getArticleTagInputValue(articleId);
    if (text.length !== 0) {
        let xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState === 4 && this.status === 200) {
                document.getElementById("tags-"+articleId).appendChild(createTagDiv(text));
            }
        };
        xmlhttp.open("POST", "post.php", true);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlhttp.send('action=add-tag&name='+text+'&articleId='+articleId);
    }
}

function getArticleTagInputValue(articleId) {
    return document.getElementById("tag-input-"+articleId).value;
}

function createTagDiv(text) {
    const tagDiv = document.createElement('div');
    tagDiv.classList.add('tag');
    tagDiv.innerHTML = text;
    return tagDiv;
}


document.addEventListener("DOMContentLoaded", function(){
    document.getElementById("tag-search").oninput = function() {
        searchTag(this);
    };
});