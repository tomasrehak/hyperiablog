function sendComment(articleId, event) {
    event.preventDefault();
    const element = document.getElementById('write-comment');
    const text = element.value;
    element.value = '';
    const commentName = document.getElementById('comment-name').value;
    if (text.length > 0 && commentName.length > 0) {
        let xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                var data = JSON.parse(xmlhttp.responseText);
                createCommentBlock(data);
            }
        };
        xmlhttp.open("POST", "post.php", true);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlhttp.send('action=send-comment&text='+text+'&articleId='+articleId+'&name='+commentName);
    }
}

function createCommentBlock(data) {
    const commentsElement = document.getElementById('comments');
    let comment = `<div class="comment">
        <div class="comment-title">${data.name} wrote:</div>
        <p>${data.content}</p>
        <div class="comment-info">at ${data.created_at}</div>
    </div>`;
    let temp = document.createElement('div');
    temp.innerHTML = comment;
    const htmlObject = temp.firstChild;
    commentsElement.appendChild(htmlObject);
}