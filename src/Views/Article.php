<div class="content">
    <?php
    $articleId = $_GET['id'];
    $blogController = new \Blog\Controllers\Blog();
    $article = $blogController->getArticle($articleId);
    ?>
    <?php if (!$article || !isset($article)) { ?>
        <h2>Article not found</h2>
    <?php } ?>
    <?php if ($article && isset($article)) { ?>
        <?php if ($article->image && isset($article->image)) { ?>
            <img src="images/<?php echo $article->image; ?>"/>
        <?php } ?>
        <h2><?= stripslashes($article->title) ?></h2>
        <p><?= stripslashes($article->content) ?></p>
        <form>
            <label>Add tag: <input type="text" id="tag-input-<?= $article->id ?>"></label>
            <button onclick="addTag(<?= $article->id ?>, event)">add</button>
        </form>
        <span id="new-tag-response"></span>
        <div id="tags-<?= $article->id ?>" class="tags">
            <div>Tags:</div>
            <?php if ($article->tags && !empty($article->tags)) : ?>
                <?php foreach ($article->tags as $key => $tag) : ?>
                    <div class="tag"><?= $tag->name ?></div>
                <?php endforeach ?>
            <?php endif ?>
        </div>
        <div class="article-info">created <?= date($article->created_at) ?>
            by <?= stripslashes($article->author) ?></div>
        <h2>Write comment:</h2>
        <form>
            <label>Your name: <input type="text" id="comment-name"/></label>
            <label class="text-label">Comment: <textarea id="write-comment" class="write-comment"></textarea></label>
            <button onclick="sendComment(<?= $article->id ?>, event)">Comment</button>
        </form>
        <div id="comments" class="comments">
            <?php
            $comments = $blogController->getCommentsForArticle($articleId);
            ?>
            <?php if ($comments && !empty($comments)) { ?>
                <?php foreach ($comments as $key => $comment) : ?>
                    <div id="comment-<?= $comment->id ?>" class="comment">
                        <div class="comment-title"><?= $comment->name ?> wrote:</div>
                        <p><?= $comment->content ?></p>
                        <div class="comment-info">at <?= $comment->created_at ?></div>
                    </div>
                <?php endforeach ?>
            <?php } ?>
        </div>
    <?php } ?>
    <script src="js/comment.js"></script>
</div>
