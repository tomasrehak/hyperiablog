<div class="content">
    <?php
    $blogController = new \Blog\Controllers\Blog();
    $articles = $blogController->getArticles();
    //echo print_r($articles, true);
    ?>
    <form>
        <label>Search tag: <input type="text" id="tag-search"/></label>
    </form>
    <?php if ($articles && !empty($articles)) : ?>
        <?php foreach ($articles as $key => $article) : ?>
            <div id="article-<?= $article->id ?>" class="article">
                <h2><a href="/article?id=<?= $article->id ?>"><?= stripslashes($article->title) ?></a></h2>
                <p><?= stripslashes($article->short_text) ?></p>
                <div id="tags-<?= $article->id ?>" class="tags">
                    <div>Tags:</div>
                    <?php if ($article->tags && !empty($article->tags)) : ?>
                        <?php foreach ($article->tags as $key => $tag) : ?>
                            <div class="tag"><?= $tag->name ?></div>
                        <?php endforeach ?>
                    <?php endif ?>
                </div>
                <div class="article-info">created <?= date($article->created_at) ?>
                    by <?= stripslashes($article->author) ?></div>
            </div>
        <?php endforeach ?>
    <?php endif ?>
    <script src="js/blog.js"></script>
</div>
