<?php
namespace Blog\Models;

use PDO;

class Article extends \Blog\Model
{
    /**
     * @return array
     */
    public static function getAll()
    {
        $db = static::getDB();
        $stmt = $db->query("SELECT id, author_id, title, short_text, content, created_at FROM article");
        return $stmt->fetchAll(PDO::FETCH_OBJ);
    }

    /**
     * @return \stdClass
     */
    public static function getById($id)
    {
        $db = static::getDB();
        $query = "SELECT id, author_id, title, short_text, content, image, created_at FROM article WHERE id = :id";
        $stmt = $db->prepare($query);
        $stmt->execute(array(":id" => $id));
        return $stmt->fetch(PDO::FETCH_OBJ);
    }
}