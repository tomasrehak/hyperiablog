<?php
namespace Blog\Models;

use PDO;

class Tag extends \Blog\Model
{
    /**
     * @return array
     */
    public static function getAll()
    {
        $db = static::getDB();
        $stmt = $db->query("SELECT id, name FROM tag");
        return $stmt->fetchAll(PDO::FETCH_OBJ);
    }

    public static function getByName($name)
    {
        $db = static::getDB();
        $stmt = $db->prepare("SELECT id, name FROM tag WHERE name = :name");
        $stmt->execute(array(":name" => $name));
        return $stmt->fetch(PDO::FETCH_OBJ);
    }

    public static function searchArticlesByTag($name) {
        $db = static::getDB();
        $stmt = $db->prepare("SELECT article_id FROM article_has_tag JOIN tag ON (id = tag_id) WHERE name LIKE :name");
        $stmt->execute(array(":name" => "%".$name."%"));
        return $stmt->fetchAll(PDO::FETCH_OBJ);
    }

    public static function createAndAssignToArticle($name, $articleId) {
        $db = static::getDB();
        try {
            $db->beginTransaction();
            $tag = self::getByName($name);
            if ($tag && isset($tag)) {
                $tagId = $tag->id;
            } else {
                $tagId = self::create($name);
            }
            self::assignToArticle($tagId, $articleId);
            $db->commit();
            return true;
        } catch (PDOExecption $e) {
            $db->rollback();
            print "Error!: " . $e->getMessage() . "</br>";
        }
        return false;
    }

    private static function create($name)
    {
        $db = static::getDB();
        $stmt = $db->prepare("INSERT IGNORE INTO tag (name) VALUES (:name)");
        $stmt->execute(array(":name" => $name));
        return $db->lastInsertId();
    }

    private static function assignToArticle($tagId, $articleId)
    {
        $db = static::getDB();
        $stmt = $db->prepare("INSERT IGNORE INTO article_has_tag (article_id, tag_id) VALUES (:article_id, :tag_id)");
        $stmt->execute(array(":article_id" => $articleId, ":tag_id" => $tagId));
    }

    /**
     * @return array
     */
    public static function getTagsForArticle($articleId)
    {
        $db = static::getDB();
        $stmt = $db->prepare("SELECT id, name FROM article_has_tag LEFT JOIN tag ON (id = tag_id) WHERE article_id = :article_id");
        try {
            $stmt->execute(array(":article_id" => $articleId));
            return $stmt->fetchAll(PDO::FETCH_OBJ);
        } catch (PDOExecption $e) {
            print "Error!: " . $e->getMessage() . "</br>";
        }
        return array();
    }
}