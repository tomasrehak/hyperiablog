<?php
namespace Blog\Models;

use PDO;

class Comment extends \Blog\Model
{
    /**
     * @return array
     */
    public static function getAll()
    {
        $db = static::getDB();
        $stmt = $db->query("SELECT id, name, created_at, content FROM comment");
        return $stmt->fetchAll(PDO::FETCH_OBJ);
    }

    /**
     * @param $id
     * @return \stdClass
     */
    public static function getById($id)
    {
        $db = static::getDB();
        $query = "SELECT id, name, created_at, content FROM comment WHERE id = :id";
        $stmt = $db->prepare($query);
        $stmt->execute(array(":id" => $id));
        return $stmt->fetch(PDO::FETCH_OBJ);
    }

    /**
     * @param $commentId
     * @return array
     */
    public static function getCommentsForComment($commentId)
    {
        $db = static::getDB();
        $stmt = $db->prepare("SELECT id, name, created_at, content FROM comment_has_comment LEFT JOIN comment ON(comment_id = id) WHERE comment_id = :comment_id");
        $stmt->execute(array(":comment_id" => $commentId));
        return $stmt->fetchAll(PDO::FETCH_OBJ);
    }

    /**
     * @param $articleId
     * @return array
     */
    public static function getAllForArticle($articleId)
    {
        $db = static::getDB();
        $stmt = $db->prepare("SELECT id, name, created_at, content FROM article_has_comment LEFT JOIN comment ON(comment_id = id) WHERE article_id = :articleId");
        $stmt->execute(array(":articleId" => $articleId));
        return $stmt->fetchAll(PDO::FETCH_OBJ);
    }


    public static function createAndAssignToComment($name, $comment, $commentId) {
        $db = static::getDB();
        try {
            $db->beginTransaction();
            $newCommentId = self::create($name, $comment);
            self::assignToComment($newCommentId, $commentId);
            $db->commit();
            return $newCommentId;
        } catch (PDOExecption $e) {
            $db->rollback();
            print "Error!: " . $e->getMessage() . "</br>";
        }
        return null;
    }

    public static function createAndAssignToArticle($name, $comment, $articleId) {
        $db = static::getDB();
        try {
            $db->beginTransaction();
            $commentId = self::create($name, $comment);
            self::assignToArticle($commentId, $articleId);
            $db->commit();
            return $commentId;
        } catch (PDOExecption $e) {
            $db->rollback();
            print "Error!: " . $e->getMessage() . "</br>";
        }
        return null;
    }

    private static function create($name, $comment)
    {
        $db = static::getDB();
        $stmt = $db->prepare("INSERT IGNORE INTO comment (name, content, created_at) VALUES (:name, :content, NOW())");
        $stmt->execute(array(":name" => $name, ":content" => $comment));
        return $db->lastInsertId();
    }

    private static function assignToArticle($commentId, $articleId)
    {
        $db = static::getDB();
        $stmt = $db->prepare("INSERT IGNORE INTO article_has_comment (article_id, comment_id) VALUES (:article_id, :comment_id)");
        $stmt->execute(array(":article_id" => $articleId, ":comment_id" => $commentId));
    }

    private static function assignToComment($newCommentId, $commentId)
    {
        $db = static::getDB();
        $stmt = $db->prepare("INSERT IGNORE INTO comment_has_comment (comment_id, comment_id2) VALUES (:comment_id, :comment_id2)");
        $stmt->execute(array(":comment_id" => $commentId, ":comment_id2" => $newCommentId));
    }
}