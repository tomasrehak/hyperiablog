<?php
namespace Blog\Models;

use PDO;

class Author extends \Blog\Model
{
    /**
     * @return array
     */
    public static function getAll()
    {
        $db = static::getDB();
        $stmt = $db->query("SELECT id, name FROM author");
        return $stmt->fetchAll(PDO::FETCH_OBJ);
    }

    /**
     * @return \stdClass
     */
    public static function getById($id)
    {
        $db = static::getDB();
        $stmt = $db->prepare("SELECT id, name FROM author WHERE id = :id");
        $stmt->execute(array(":id" => $id));
        return $stmt->fetch(PDO::FETCH_OBJ);
    }
}