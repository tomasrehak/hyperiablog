<?php
namespace Blog\Controllers;

use Blog\Models\Article;
use Blog\Models\Author;
use Blog\Models\Comment;
use Blog\Models\Tag;

class Blog
{
    public function getArticle($articleId) {
        $article = Article::getById($articleId);
        $article = $this->mapArticleAuthor($article);
        $article = $this->mapArticleTags($article);
        return $article;
    }

    public function getArticles() {
        $articles = Article::getAll();
        $articles = array_map(array($this, "mapArticleAuthor"), $articles);
        $articles = array_map(array($this, "mapArticleTags"), $articles);
        return $articles;
    }

    public function createTagForArticle($tagName, $articleId) {
        return Tag::createAndAssignToArticle($tagName, $articleId);
    }

    public function searchArticlesByTag($tagName) {
        return Tag::searchArticlesByTag($tagName);
    }

    private function mapArticleAuthor($article) {
        $author = Author::getById(+$article->author_id);
        $article->author = $author->name;
        return $article;
    }

    private function mapArticleTags($article) {
        $article->tags = Tag::getTagsForArticle($article->id);
        return $article;
    }

    public function getCommentsForArticle($articleId) {
        $comments = Comment::getAllForArticle($articleId);
        $comments = array_map(array($this, "mapCommentsComments"), $comments);
        return $comments;
    }

    //TODO recursion
    private function getCommentsForComment($forComment) {
        $comments = Comment::getCommentsForComment(+$forComment->id);
        if (empty($comments)) {
            return;
        }
        foreach ($comments as $comment) {
            return $this->getCommentsForComment($comment);
        }
    }

    private function mapCommentsComments($comment) {
        $comment->comments = $this->getCommentsForComment($comment);
        return $comment;
    }

    public function saveCommentToArticle($name, $comment, $articleId) {
        $commentId = Comment::createAndAssignToArticle($name, $comment, $articleId);
        return Comment::getById($commentId);
    }

    public function saveCommentToComment($name, $comment, $commentId) {
        $commentId = Comment::createAndAssignToComment($name, $comment, $commentId);
        return Comment::getById($commentId);
    }
}
